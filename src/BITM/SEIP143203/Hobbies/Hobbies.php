<?php
namespace App\BITM\SEIP143203\Hobbies;
use App\BITM\SEIP143203\Message\Message;
use App\BITM\SEIP143203\Utility\Utility;
use App\BITM\SEIP143203\Model\Database as DB;
use PDO;

class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies="";



    public function __construct(){
        parent::__construct();
    }


        public function index($fetchMode='ASSOC'){
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from hobbies where is_deleted='0'");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_hobbies=$sth->fetchAll();


            return  $all_hobbies;
        }









public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from hobbies Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_person=$sth->fetch();

        return  $selected_person;

    }


    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->hobbies);

        //var_dump($values);


        $query='UPDATE hobbies  SET  name = ?   , hobbies= ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ name: $this->name ] ,
               [ Hobbies: $this->hobbies ] <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from hobbies Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }


    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('hobbies',$data)){
            $this->hobbies=$data['hobbies'];

        }
        if(array_key_exists('checkbox',$data))
        {
            $this->checkbox = $data['checkbox'];
        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->hobbies);
        $query="insert into hobbies(name,hobbies) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");
else
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [Hobbies: $this->hobbies ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");

        Utility::redirect('create.php');



    }


    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE hobbies  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }

    //method 8
    public function trashed($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from hobbies WHERE is_deleted='1'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $allData=$sth->fetchAll();

        return  $allData;
    }//end of trashed method which show the trashed list



    public function count($fetchMode='ASSOC'){
        $DBH=$this->connection;
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project_b37`.`hobbies` WHERE `is_deleted`=0 ";
        $sth=$DBH->prepare($query);
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $row=$sth->fetchAll();


        /* $result=mysqli_query($this->conn,$query);*/
        /* $row= mysqli_fetch_assoc($sth);*/
        return $row['totalItem'];
    }//end of count

    public function mulSoftDelete()

    {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox);$i++) {
            $query = $this->connection->prepare("UPDATE hobbies SET is_deleted='1' WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();
            if ($query) {
                Message::message("<div class='alert alert-success' id='msg'><h3 align='center'> Data Has Been Deleted Successfully!</h3></div>");

            } else {
                Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'> Data Has Not Been Deleted Successfully!</h3></div>");

            }
            Utility::redirect("multipleDelete.php");
        }
    }//end of multiple trash delete

    public function deleteMultiple($id)
    {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox);$i++)
        {

            $query =$this->connection->prepare("delete  from hobbies WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();

            if ($query)
            {
                Message::message("<div class=\"alert alert-info\"><strong>Deleted!</strong> Selected Data has been deleted successfully.</div>");
                Utility::redirect("index.php");
            }
            else
            {
                Message::message("<div class=\"alert alert-info\"> <strong>Deleted!</strong> Selected Data has not been deleted successfully.</div>");
                Utility::redirect("index.php");
            }

        }
    }



    public function recover($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE hobbies SET is_deleted  = "0" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been recovered Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
        Utility::redirect('recovery.php');



    }///EOF recover

    public function mulRecover()

    {
        $checkbox = $_POST['checkbox'];
        /* var_dump($checkbox);die();*/
        for ($i = 0; $i < count($checkbox);$i++) {
            $query = $this->connection->prepare("UPDATE hobbies SET is_deleted='0' WHERE id='$checkbox[$i]'");
            var_dump($query);
            $query->execute();
            if ($query) {
                Message::message("<div class='alert alert-success' id='msg'><h3 align='center'> Data Has Been Recoverd Successfully!</h3></div>");

            } else {
                Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Not Been Deleted Successfully!</h3></div>");

            }
            Utility::redirect("recovery.php");
        }
    }//EOF mulRecover


    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from hobbies  WHERE is_deleted = '0' LIMIT $start,$itemsPerPage";

        $STH = $this->connection->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator()



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) )  $sql = "SELECT * FROM `hobbies` WHERE `is_deleted` ='0' AND (`name` LIKE '%".$requestArray['search'].$requestArray['search']."%')";
        if(isset($requestArray['byName'])  ) $sql = "SELECT * FROM `hobbies` WHERE `is_deleted` ='0' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) )  $sql = "SELECT * FROM `hobbies` WHERE `is_deleted` ='0' AND `name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `hobbies` WHERE `is_deleted` ='0'";

        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        /*// for each search field block start
        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $STH = $this->connection->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $all_books= $STH->fetchAll();
        foreach ($all_books as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end*/


        return array_unique($_allKeywords);


    }// get all keywords





}

