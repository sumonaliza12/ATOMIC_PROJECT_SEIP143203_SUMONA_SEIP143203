<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143203\SummaryOfOrganization;
use App\BITM\SEIP143203\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>


    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="js/index.js"></script>
    <link rel="stylesheet" href="../../../resource/assets2/css/style.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <style>
        body{background-image: url(../../../resource/assets/images/backgrounds/1.jpg) !important;}
    </style>
</head>

<body>

<h1 style="text-align: center;"><b>Organization List<b/></h1>

<table class="table" style="width:800px;margin:0 auto; ">

    <?php
    $obj = new SummaryOfOrganization\SummaryOfOrganization();
    $allData = $obj->trashed("obj");
    ?>
    <tbody>
    <tr class="danger">
        <td></td>
        <td>Serial</td>
        <td>Name</td>
        <td>Gender</td>
        <td>Action</td>
    </tr>
    <?php
    $count = 1;
    echo '
          <a href="index.php"><button class="btn btn-success">Home</button></a>
          <a href="multipleDelete.php"><button class="btn btn-danger">Delete</button></a>

 <form action="recover.php" method="post" class="form-group">
 <button type="submit" class="btn btn-info">Recover Selected</button>';

    foreach($allData as $data){
        if($count%2==0) $class ="info" ;
        else $class="success" ;
        echo'

                   <tr class="'.$class.'">

                       <input name="BookId" type="hidden" value="'.$data->id.'">
                       <td align="center" bgcolor="#FFFFFF"><input name="checkbox[]" type="checkbox" value="'.$data->id.'"></td>

                       <td>'.$count.'</td>
                       <td>'.$data->organization_name.'</td>
                       <td>'.$data->organization_summary.'</td>
                       <td>
                       <div class="btn-group">

                         <a href="recover.php?id='.$data->id.'"><button type="button" class="btn btn-success">Recover</button></a>

                        </div>
                        </td>

                   </tr>
                     </form> ';

        $count++;

    }


    ?>


    </tbody>
</table>


<script>
    function removeFadeOut( el, speed ) {
        var seconds = speed/1000;
        el.style.transition = "opacity "+seconds+"s ease";

        el.style.opacity = 0;
        setTimeout(function() {
            el.parentNode.removeChild(el);
        }, speed);
    }

    removeFadeOut(document.getElementById('msg'), 9000);
</script>

<![endif]-->

</body>

</html>
