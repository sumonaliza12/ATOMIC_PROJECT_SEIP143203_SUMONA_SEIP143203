<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143203\Message\Message;
if(!isset( $_SESSION)) session_start();
$message1=Message::message();



use App\BITM\SEIP143203\Hobbies\Hobbies;

$obj= new Hobbies();


$allData= $obj->index("obj");

######################## pagination code block#1 of 2 start ######################################

$recordCount= count($allData);


if(isset($_REQUEST['Page']))
    $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))
    $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))
    $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))
    $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;

$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $obj->search($_REQUEST);
$availableKeywords=$obj->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################



################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $obj->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################



?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/Bootstrap/css/jquery-ui.css">
    <script src="../../../resource/Bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/Bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->

</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/general3.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >


<table>
    <tr>
        <td width="500">

        </td>
        <td width="450">
            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>
        </td>
        <td width="150">


            <!-- required for search, block 4 of 5 start -->


            <form id="searchForm" action="index.php"  method="get">
                <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
                <input type="checkbox"  name="byName"   checked  >By  Name
                <!-- <input type="checkbox"  name="byAuthor"  checked >By Author-->
                <input hidden type="submit" class="btn-primary" value="search" >
            </form>

            <!-- required for search, block 4 of 5 end -->


        </td>
    </tr>
</table>

<div class="container ">
    <div style="margin-top: 40px ;float: right;"> <a href="../index.php" class="btn btn-danger custom btn-lg" role="button">Atomic Project List</a></br></br></div>


    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> Hobbies List</h1>


                </div>
            </div>





            <div class="panel-body">
                <div class="table-responsive" >
                    </br></br></br></br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gender</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial=0;

                            foreach($someData as $oneData){
                            $serial++; ?>
                            <td><?php echo $serial?></td>
                            <td><?php echo $oneData->id ?></td>
                            <td><?php echo $oneData->name?></td>
                            <td><?php echo $oneData->hobbies ?></td>
                            <td><a href="view.php?id=<?php echo $oneData->id  ?>" class="btn btn-primary" role="button">View</a>
                                <a href="edit.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button">Edit</a>
                                <a href="delete.php?id=<?php echo $oneData->id ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                                <a href="trash.php?id=<?php echo $oneData->id ?>"  class="btn btn-info" role="button">Trash</a>
                            </td>

                        </tr>
                        <?php }?>


                        <a href="create.php"   class="btn btn-primary role="button">  Add new Hobbies</a> &nbsp;&nbsp;&nbsp;
                        <a href="multipleTrash.php " class="btn btn-info role=" button">Multiple Trash </a>
                        <a href="trashed.php "  class="btn btn-primary role="button"> View Trash List </a>
                        <a href="multipleDelete.php "  class="btn btn-danger role="button"> Multiple Delete </a>


                        </tbody>
                    </table>
                    <div id="confirmation_message" style="color:red;">
                        <?php echo $message1 ?>
                    </div>

                </div>

            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });
</script>



<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="left" class="container">
    <ul class="pagination">

        <?php
        $pageMinusOne = $page -1;
        if($page>1) echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . '</a></li>';
        //echo '<li><a href="">' . "Previous" . '</a></li>';
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        $pagePlusOne=$page+1;
        echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . '</a></li>';
        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3" style="float: right">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4" style="float: right">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5" style="float: right">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6" style="float: right">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10" style="float: right">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15" style="float: right">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->



</body>
</html>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->





